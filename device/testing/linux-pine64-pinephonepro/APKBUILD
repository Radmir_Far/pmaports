# Maintainer: Martijn Braam <martijn@brixit.nl>

pkgname=linux-pine64-pinephonepro
pkgver=5.16.7
pkgrel=0
pkgdesc="Mainline kernel for the pinephone pro"
arch="aarch64"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	gzip
	linux-headers
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
	rsync
	xz
	"

case "$CARCH" in
	aarch64*) _carch="arm64" ;;
	arm*) _carch="arm" ;;
esac

# Source
_config="config-$_flavor.$CARCH"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
_commit="138b2bb2032e6df014efe88e4a4bbdd4ad93b18d"
source="
	https://gitlab.com/pine64-org/linux/-/archive/ppp-$pkgver/linux-ppp-$pkgver.tar.gz
	config-$_flavor.aarch64
	"
builddir="$srcdir/linux-ppp-$pkgver"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))"

}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs-$_flavor
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}


sha512sums="
365697049c59f2d0baf27becd6d00da030ab931b16ef075ca9a227302adf741de55137a349d589edd8bd028f95f6e23417858cfefb9d9748ec5e36fc3b4205de  linux-ppp-5.16.7.tar.gz
1fc43a1bca4cb2a82617568cee24d49acc172ddc8fcc2d652760c368b242e536629b6e0fda5f38b990ee2e5fb029c1592ea5bdd0f89a83bd3484bc8d533d64cc  config-pine64-pinephonepro.aarch64
"
